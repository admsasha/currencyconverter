#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <memory>

#include <QMainWindow>
#include <QSharedPointer>
#include <QVector>
#include <QSettings>
#include <QSqlQueryModel>

#include "Banks/IBank.h"
#include "FormWait.h"


namespace Ui {
    class MainWindow;
}

class MainWindow : public QMainWindow {
    Q_OBJECT

    public:
        explicit MainWindow(QWidget *parent = nullptr);
        ~MainWindow();

    private:
        Ui::MainWindow *ui;

        int indexCurrentBank;
        QVector<QSharedPointer<IBank>> listBanks;

        FormWait *formWait;

        QSqlQueryModel *model;

        QSettings *confSettings;

        QMap<QString,bool> listCodeCurrencyForVisible;

        void initDatabases();
        void initComboBanks();

        bool isBlockUpdateSpin;

    private slots:
        void ComboBox3CurrentIndexChanged(int);
        void ComboBoxCurrentIndexChanged(int);
        void getCourses();

        void doubleSpinBox1ValueChanged(double);
        void doubleSpinBox2ValueChanged(double);

        void tableViewCurrentChanged();

        void showFormAbout();
        void showFormVisibilityCurrency();
        void showFormDenialResponsibility();
};

#endif // MAINWINDOW_H
