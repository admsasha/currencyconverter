#ifndef TRANSLATECODECURRENCY_H
#define TRANSLATECODECURRENCY_H

#include <QString>
#include <QMap>

class TranslateCodeCurrency {
    public:
        TranslateCodeCurrency();

        QString CodeToString(QString code);
        QStringList getAllCodes();

    private:
        QMap<QString,QString> listCodes;

};

#endif // TRANSLATECODECURRENCY_H
