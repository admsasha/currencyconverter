#include "MainWindow.h"
#include "ui_MainWindow.h"


#include <QtDebug>
#include <QSqlDatabase>
#include <QSqlQuery>
#include <QSqlError>
#include <QStandardPaths>
#include <QDir>
#include <QTableWidget>

#include "Banks/BankRu.h"
#include "Banks/BankUa.h"
#include "Banks/BankBy.h"

#include "config_app.h"
#include "FormAbout.h"
#include "FormVisibilityCurrency.h"
#include "TranslateCodeCurrency.h"
#include "FormDenialResponsibility.h"


MainWindow::MainWindow(QWidget *parent) : QMainWindow(parent),    ui(new Ui::MainWindow) {
    ui->setupUi(this);

    this->setWindowTitle(tr("Currency converter v%1").arg(QString(APP_VERSION)));

    indexCurrentBank=-1;
    isBlockUpdateSpin=false;

    confSettings = new QSettings (QStandardPaths::writableLocation(QStandardPaths::AppConfigLocation)+"/settings.ini", QSettings::IniFormat);
    confSettings->setPath(QSettings::IniFormat, QSettings::UserScope, QDir::currentPath());

    initDatabases();

    model = new QSqlQueryModel;
    ui->tableView->setModel(model);


    formWait = new FormWait(this);
    formWait->move((this->width()-formWait->width())/2,(this->height()-formWait->height())/2);
    formWait->hide();


    listBanks.push_back(QSharedPointer<IBank>(new BankRu()));
    listBanks.push_back(QSharedPointer<IBank>(new BankBy()));
    listBanks.push_back(QSharedPointer<IBank>(new BankUa()));

    ui->dateEdit->setDate(QDate::currentDate());

    connect(ui->pushButton_2,&QAbstractButton::clicked,this,&MainWindow::getCourses);
    connect(ui->comboBox_3,static_cast<void(QComboBox::*)(int)>(&QComboBox::currentIndexChanged),this,&MainWindow::ComboBox3CurrentIndexChanged);
    connect(ui->comboBox,static_cast<void(QComboBox::*)(int)>(&QComboBox::currentIndexChanged),this,&MainWindow::ComboBoxCurrentIndexChanged);
    connect(ui->doubleSpinBox,static_cast<void(QDoubleSpinBox::*)(double)>(&QDoubleSpinBox::valueChanged),this,&MainWindow::doubleSpinBox1ValueChanged);
    connect(ui->doubleSpinBox_2,static_cast<void(QDoubleSpinBox::*)(double)>(&QDoubleSpinBox::valueChanged),this,&MainWindow::doubleSpinBox2ValueChanged);
    connect(ui->tableView->selectionModel(), &QItemSelectionModel::currentChanged, this, &MainWindow::tableViewCurrentChanged);

    connect(ui->actionAbout,&QAction::triggered,this,&MainWindow::showFormAbout);
    connect(ui->actionExit,&QAction::triggered,this,&MainWindow::close);
    connect(ui->actionVisibility_of_the_currency,&QAction::triggered,this,&MainWindow::showFormVisibilityCurrency);
    connect(ui->actionDenial_of_responsibility,&QAction::triggered,this,&MainWindow::showFormDenialResponsibility);

    initComboBanks();
}

MainWindow::~MainWindow(){
    delete ui;
}

void MainWindow::initDatabases(){
    QSqlDatabase db = QSqlDatabase::addDatabase("QSQLITE","currencyconverter");
    db.setDatabaseName(QStandardPaths::writableLocation(QStandardPaths::AppDataLocation)+"/databases/currencyconverter.db");
    if (!db.open()) {
        qDebug() << "Unable to establish a database connection.";
    }
    QSqlQuery query(QSqlDatabase::database("currencyconverter"));

    query.exec("CREATE TABLE IF NOT EXISTS exchange_rate ("
     "`id` INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL ,"
     "`code_bank` TEXT,"
     "`date` DATE,"
     "`currency_code` varchar(3),"
     "`nominal` INTEGER,"
     "`rate` REAL"
     ")");

    query.exec("CREATE INDEX IF NOT EXISTS code_bank on exchange_rate(code_bank)");
}

void MainWindow::initComboBanks(){
    ui->comboBox_3->clear();
    for (auto &x:listBanks) ui->comboBox_3->addItem(x->name(),x->getCodeBank());
    ui->comboBox_3->setCurrentIndex(0);
}

void MainWindow::ComboBox3CurrentIndexChanged(int){
    indexCurrentBank = ui->comboBox_3->currentIndex();
    QString codeBank = listBanks.at(indexCurrentBank)->getCodeBank();

    QSqlQuery query(QSqlDatabase::database("currencyconverter"));

    ui->label_5->setText(listBanks.at(indexCurrentBank)->currency());

    QString currentDate = ui->dateEdit->date().toString("yyyy-MM-dd");

    QString currentSelectCurrencyCode = "";

    if (ui->comboBox->currentIndex()!=-1){
        currentSelectCurrencyCode = ui->comboBox->currentData().toString();
    }

    TranslateCodeCurrency trCode;

    // get currency exchange
    ui->comboBox->clear();

    query.prepare("SELECT `currency_code` FROM exchange_rate WHERE code_bank=:code_bank "
                  "and `date`>DATE(:date,'-30 day') "
                  "and `date`<DATE(:date,'+30 day') group by `currency_code`");
    query.bindValue(":code_bank",codeBank);
    query.bindValue(":date",currentDate);
    if(!query.exec()){
        qDebug() << query.lastError();
        return;
    }
    while (query.next()) {
        QString currency_code = query.value(0).toString();

        if (confSettings->value("visibility_codes/"+currency_code,"true").toString()!="true") continue;


        ui->comboBox->addItem(trCode.CodeToString(currency_code),currency_code);

        if (currency_code==currentSelectCurrencyCode){
            ui->comboBox->setCurrentIndex(ui->comboBox->count()-1);
        }
    }
    if (ui->comboBox->currentIndex()==-1) ui->comboBox->setCurrentIndex(0);
    query.clear();

}

void MainWindow::ComboBoxCurrentIndexChanged(int){
    QSqlQuery query(QSqlDatabase::database("currencyconverter"));

    indexCurrentBank = ui->comboBox_3->currentIndex();
    QString codeBank = listBanks.at(indexCurrentBank)->getCodeBank();
    QString currentDate = ui->dateEdit->date().toString("yyyy-MM-dd");

    QString currentSelectDate = ui->tableView->model()->data(ui->tableView->model()->index(ui->tableView->currentIndex().row(),1)).toString();

    QString sqlQuery = "SELECT `id`,`date`,`nominal`,`rate` from exchange_rate "
                       "WHERE code_bank='"+codeBank+"' "
                            "and currency_code='"+ui->comboBox->currentData().toString()+"' "
                            "and `date`>DATE('"+currentDate+"','-15 day') "
                            "and `date`<DATE('"+currentDate+"','+15 day') order by date desc";

     model->setQuery(sqlQuery,QSqlDatabase::database("currencyconverter"));

     model->setHeaderData(1, Qt::Horizontal, tr("Date"));
     model->setHeaderData(2, Qt::Horizontal, tr("Nominal"));
     model->setHeaderData(3, Qt::Horizontal, tr("Rate"));
     ui->tableView->setModel(model);

     ui->tableView->setColumnHidden(0,1);
     ui->tableView->setCurrentIndex(ui->tableView->model()->index(0,1));

     // Возрат сохраненной даты
     for (int i=0;i<ui->tableView->model()->rowCount();i++){
         QString row_date = ui->tableView->model()->data(ui->tableView->model()->index(i,1)).toString();
         if (row_date==currentSelectDate){
             ui->tableView->setCurrentIndex(ui->tableView->model()->index(i,1));
             ui->tableView->selectRow(i);
         }
     }

     ui->tableView->horizontalHeader()->setSectionResizeMode(1,QHeaderView::ResizeToContents);
     ui->tableView->horizontalHeader()->setSectionResizeMode(2,QHeaderView::ResizeToContents);
     ui->tableView->horizontalHeader()->setSectionResizeMode(3,QHeaderView::Stretch);
}

void MainWindow::getCourses(){
    if (indexCurrentBank==-1) return;
    QSqlQuery query(QSqlDatabase::database("currencyconverter"));

    QString codeBank = listBanks.at(indexCurrentBank)->getCodeBank();
    QString currentDate = ui->dateEdit->date().toString("yyyy-MM-dd");

    query.prepare("SELECT count(`id`) FROM exchange_rate WHERE code_bank=:code_bank and `date`=DATE(:date)");
    query.bindValue(":code_bank",codeBank);
    query.bindValue(":date",currentDate);
    if(!query.exec()) return;
    query.first();
    if (query.value(0).toInt()==0){
        formWait->showDialog(tr("Loading courses"),tr("Exchange rates are being downloaded. Please wait..."));
        listBanks.at(indexCurrentBank)->getCourses(ui->dateEdit->date());
        formWait->hide();
    }
    query.clear();

    ComboBox3CurrentIndexChanged(0);

    // Установка выбранной даты
    for (int i=0;i<ui->tableView->model()->rowCount();i++){
        QString row_date = ui->tableView->model()->data(ui->tableView->model()->index(i,1)).toString();
        if (row_date==ui->dateEdit->date().toString("yyyy-MM-dd")){
            ui->tableView->setCurrentIndex(ui->tableView->model()->index(i,1));
            ui->tableView->selectRow(i);
        }
    }
}

void MainWindow::doubleSpinBox1ValueChanged(double){
    if (isBlockUpdateSpin) return;

    double nominal = ui->tableView->model()->data(ui->tableView->model()->index(ui->tableView->currentIndex().row(),2)).toDouble();
    double rate = ui->tableView->model()->data(ui->tableView->model()->index(ui->tableView->currentIndex().row(),3)).toDouble();

    double value = ui->doubleSpinBox->value()/rate*nominal;

    isBlockUpdateSpin=true;
    ui->doubleSpinBox_2->setValue(value);
    isBlockUpdateSpin=false;
}

void MainWindow::doubleSpinBox2ValueChanged(double){
    if (isBlockUpdateSpin) return;
    double nominal = ui->tableView->model()->data(ui->tableView->model()->index(ui->tableView->currentIndex().row(),2)).toDouble();
    double rate = ui->tableView->model()->data(ui->tableView->model()->index(ui->tableView->currentIndex().row(),3)).toDouble();

    double value = ui->doubleSpinBox_2->value()/nominal*rate;

    isBlockUpdateSpin=true;
    ui->doubleSpinBox->setValue(value);
    isBlockUpdateSpin=false;
}

void MainWindow::tableViewCurrentChanged(){
    doubleSpinBox1ValueChanged(0);
    ui->groupBox->setTitle(tr("express converter at the rate of")+" "+ui->tableView->model()->data(ui->tableView->model()->index(ui->tableView->currentIndex().row(),1)).toString());
}

void MainWindow::showFormAbout(){
    FormAbout form;
    form.exec();
}

void MainWindow::showFormVisibilityCurrency(){
    FormVisibilityCurrency form;
    form.exec();

    ComboBox3CurrentIndexChanged(0);
}

void MainWindow::showFormDenialResponsibility(){
    FormDenialResponsibility form;
    form.exec();
}
