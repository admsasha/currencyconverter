#ifndef FORMWAIT_H
#define FORMWAIT_H

#include <QWidget>

namespace Ui {
class FormWait;
}

class FormWait : public QWidget {
    Q_OBJECT

    public:
        explicit FormWait(QWidget *parent = nullptr);
        ~FormWait();

        void showDialog(QString title,QString text);

    private:
        Ui::FormWait *ui;
};

#endif // FORMWAIT_H
