#include "FormSettings.h"
#include "ui_FormSettings.h"

FormSettings::FormSettings(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::FormSettings)
{
    ui->setupUi(this);
}

FormSettings::~FormSettings()
{
    delete ui;
}
