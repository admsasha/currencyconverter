Name:           currencyconverter
Version:        1.0.0
Release:        alt1
Summary:        Currency converter
Group:          Office
License:        GPLv3+
Url:            https://bitbucket.org/admsasha/currencyconverter
Source0:        %{name}-%{version}.tar

BuildRequires:	qt5-tools
BuildRequires:	pkgconfig(Qt5Core)
BuildRequires:	pkgconfig(Qt5Gui)
BuildRequires:	pkgconfig(Qt5Widgets)
BuildRequires:	pkgconfig(Qt5Network)
BuildRequires:	pkgconfig(Qt5Sql)
BuildRequires:	pkgconfig(Qt5Xml)

%description
Converter currency exchange rates

%prep
%setup -q

%build
lrelease-qt5 CurrencyConverter.pro
%qmake_qt5
%make_build

%install
%makeinstall INSTALL_ROOT=%{buildroot}

%files
%doc README.md
%{_bindir}/%{name}
%{_datadir}/%{name}
%{_datadir}/applications/%{name}.desktop
%{_iconsdir}/hicolor/*/apps/%{name}.png

%changelog
* Wed Nov 13 2019 Alexander Danilov  <admsasha@altlinux.org> 1.0.0-alt1
- release 1.0.0
