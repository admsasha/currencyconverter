#include <QApplication>
#include <QTranslator>
#include <QLibraryInfo>
#include <QDir>
#include <QStandardPaths>

#include "config_app.h"
#include "MainWindow.h"

int main(int argc, char *argv[]){
    QApplication app(argc, argv);

    QCoreApplication::setOrganizationName("DanSoft");
    QCoreApplication::setOrganizationDomain("dansoft.ru");
    QCoreApplication::setApplicationVersion(QString(APP_VERSION));
    QCoreApplication::setApplicationName("CurrencyConverter");

    // Создание рабочих каталогов
    QDir dirConfig(QStandardPaths::writableLocation(QStandardPaths::AppConfigLocation));
    if (dirConfig.exists()==false) dirConfig.mkpath(dirConfig.path());

    QDir dirAppData(QStandardPaths::writableLocation(QStandardPaths::AppDataLocation));
    if (dirAppData.exists()==false) dirAppData.mkpath(dirAppData.path());

    QDir dirAppDataDataBase(QStandardPaths::writableLocation(QStandardPaths::AppDataLocation)+"/databases");
    if (dirAppDataDataBase.exists()==false) dirAppData.mkpath(dirAppDataDataBase.path());


    // Инициализация переводов
    QTranslator appTranslator;
    QTranslator qtTranslator;
    QString locale = QLocale::system().name();

#ifdef Q_OS_WIN32
    qtTranslator.load("qt_"+locale,QString(PATH_USERDATA)+"/translations");
#else
    qtTranslator.load("qt_"+locale,QLibraryInfo::location(QLibraryInfo::TranslationsPath));
#endif
    app.installTranslator(&qtTranslator);

    appTranslator.load(QString(PATH_USERDATA)+QString("/langs/currencyconverter_") + locale);
    app.installTranslator(&appTranslator);

    MainWindow form;
    form.show();

    return app.exec();
}
