#include "FormWait.h"
#include "ui_FormWait.h"

FormWait::FormWait(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::FormWait)
{
    ui->setupUi(this);
}

FormWait::~FormWait()
{
    delete ui;
}

void FormWait::showDialog(QString title, QString text)
{
    ui->label_2->setText(title);
    ui->label->setText(text);
    show();
}
