#ifndef FORMVISIBILITYCURRENCY_H
#define FORMVISIBILITYCURRENCY_H

#include <QDialog>
#include <QSettings>

namespace Ui {
class FormVisibilityCurrency;
}

class FormVisibilityCurrency : public QDialog {
    Q_OBJECT

    public:
        explicit FormVisibilityCurrency(QWidget *parent = nullptr);
        ~FormVisibilityCurrency();


    private:
        Ui::FormVisibilityCurrency *ui;
        QSettings *confSettings;


    private slots:
        void saveAndClose();
};

#endif // FORMVISIBILITYCURRENCY_H
