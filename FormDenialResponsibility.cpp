#include "FormDenialResponsibility.h"
#include "ui_FormDenialResponsibility.h"

FormDenialResponsibility::FormDenialResponsibility(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::FormDenialResponsibility)
{
    ui->setupUi(this);
    this->setWindowTitle(tr("Вenial of responsibility"));

    connect(ui->pushButton,&QAbstractButton::clicked,this,&FormDenialResponsibility::close);

}

FormDenialResponsibility::~FormDenialResponsibility()
{
    delete ui;
}
