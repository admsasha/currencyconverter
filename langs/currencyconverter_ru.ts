<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ru_RU">
<context>
    <name>FormAbout</name>
    <message>
        <location filename="../FormAbout.ui" line="131"/>
        <source>License: GPLv3+</source>
        <translation>Лицензия: GPLv3+</translation>
    </message>
    <message>
        <location filename="../FormAbout.ui" line="208"/>
        <source>Visit web site</source>
        <translation>Посетить сайт</translation>
    </message>
    <message>
        <location filename="../FormAbout.ui" line="226"/>
        <source>Close</source>
        <translation>Закрыть</translation>
    </message>
    <message>
        <location filename="../FormAbout.cpp" line="15"/>
        <source>About</source>
        <translation>О программе</translation>
    </message>
    <message>
        <location filename="../FormAbout.cpp" line="18"/>
        <source>Currency Converter</source>
        <translation>Конвертер валют</translation>
    </message>
    <message>
        <location filename="../FormAbout.cpp" line="19"/>
        <source>Version:</source>
        <translation>Версия:</translation>
    </message>
    <message>
        <location filename="../FormAbout.cpp" line="20"/>
        <source>Date build:</source>
        <translation>Дата сборки:</translation>
    </message>
    <message>
        <location filename="../FormAbout.cpp" line="21"/>
        <source>All rights reserved.</source>
        <translation>Все права защещены.</translation>
    </message>
</context>
<context>
    <name>FormDenialResponsibility</name>
    <message>
        <location filename="../FormDenialResponsibility.ui" line="62"/>
        <source>Software are granted on an &quot;as is&quot; basis which means that the Rightholder does not provide to the User any guarantee that: currency converter will meet User&apos;s requirements,currency converter will be provided in a continuous, timely, reliable and error-free manner; results which can be received with their use will be accurate and reliable; all errors will be corrected.</source>
        <translation>Программное обеспечение предоставляется на условиях &quot;как есть&quot;, что означает, что Правообладатель не предоставляет пользователю никаких гарантий того, что: конвертер валют будет соответствовать требованиям пользователя, конвертер валют будет предоставляться непрерывно, своевременно, надежно и безошибочно; результаты, которые могут быть получены с их использованием, будут точными и надежными; все ошибки будут исправлены.</translation>
    </message>
    <message>
        <location filename="../FormDenialResponsibility.ui" line="88"/>
        <source>Accept</source>
        <translation>Применить</translation>
    </message>
    <message>
        <location filename="../FormDenialResponsibility.cpp" line="9"/>
        <source>Вenial of responsibility</source>
        <translation>Отказ от ответственности</translation>
    </message>
</context>
<context>
    <name>FormVisibilityCurrency</name>
    <message>
        <location filename="../FormVisibilityCurrency.ui" line="20"/>
        <source>Visibility of the currency</source>
        <translation>Отображаемые курсы</translation>
    </message>
    <message>
        <source>Show</source>
        <translation type="vanished">Показать</translation>
    </message>
    <message>
        <location filename="../FormVisibilityCurrency.ui" line="48"/>
        <source>Code</source>
        <translation>Код</translation>
    </message>
    <message>
        <location filename="../FormVisibilityCurrency.ui" line="53"/>
        <source>Name</source>
        <translation>Название</translation>
    </message>
    <message>
        <location filename="../FormVisibilityCurrency.ui" line="74"/>
        <source>Accept</source>
        <translation>Применить</translation>
    </message>
    <message>
        <location filename="../FormVisibilityCurrency.ui" line="81"/>
        <source>Cancel</source>
        <translation>Отмена</translation>
    </message>
    <message>
        <location filename="../FormVisibilityCurrency.cpp" line="15"/>
        <source>Visibility currency</source>
        <translation>Отображаемые курсы</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../MainWindow.ui" line="124"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;a href=&quot;http://dansoft.ru&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0000ff;&quot;&gt;© 2019 DanSoft. All rights reserved&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;a href=&quot;http://dansoft.ru&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0000ff;&quot;&gt;© 2019 DanSoft. Все права защищены&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="28"/>
        <source>Banks:</source>
        <translation>Банки:</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="110"/>
        <source>Get courses</source>
        <translation>Получить курсы</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="291"/>
        <source>File</source>
        <translation>Файл</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="297"/>
        <source>Setup</source>
        <translation>Настройки</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="303"/>
        <location filename="../MainWindow.ui" line="314"/>
        <source>About</source>
        <translation>О программе</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="319"/>
        <source>Exit</source>
        <translation>Выход</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="324"/>
        <source>Visibility of the currency</source>
        <translation>Отображаемые курсы</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="329"/>
        <source>Denial of responsibility</source>
        <translation>Отказ от ответственности</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="27"/>
        <source>Currency converter v%1</source>
        <translation>Конвертер валют v%1</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="161"/>
        <source>Date</source>
        <translation>Дата</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="162"/>
        <source>Nominal</source>
        <translation>Номинал</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="163"/>
        <source>Rate</source>
        <translation>Курс</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="196"/>
        <source>Loading courses</source>
        <translation>Загрузка курсов валют</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="196"/>
        <source>Exchange rates are being downloaded. Please wait...</source>
        <translation>Загружаются обменные курсы. Пожалуйста ждите...</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="241"/>
        <source>express converter at the rate of</source>
        <translation>Экспресс конвертирование по курсу на</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../Banks/BankBy.cpp" line="24"/>
        <source>National Bank of the Republic of Belarus</source>
        <translation>Национальный банк Республики Беларусь</translation>
    </message>
    <message>
        <location filename="../Banks/BankBy.cpp" line="40"/>
        <location filename="../Banks/BankRu.cpp" line="39"/>
        <location filename="../Banks/BankUa.cpp" line="39"/>
        <source>Currency converter</source>
        <translation>Конвертер валют</translation>
    </message>
    <message>
        <location filename="../Banks/BankBy.cpp" line="40"/>
        <location filename="../Banks/BankRu.cpp" line="39"/>
        <location filename="../Banks/BankUa.cpp" line="39"/>
        <source>is not available</source>
        <translation>не доступен</translation>
    </message>
    <message>
        <location filename="../Banks/BankBy.cpp" line="74"/>
        <location filename="../Banks/BankRu.cpp" line="88"/>
        <location filename="../Banks/BankUa.cpp" line="91"/>
        <source>Currency Converter</source>
        <translation>Конвертер валют</translation>
    </message>
    <message>
        <location filename="../Banks/BankBy.cpp" line="74"/>
        <location filename="../Banks/BankRu.cpp" line="88"/>
        <location filename="../Banks/BankUa.cpp" line="91"/>
        <source>There are no courses for this number. The Bank offers to use %1 exchange rates</source>
        <translation>Нет курсов валют на текущую дату. Банк предлагает использовать обменные  курсы на %1</translation>
    </message>
    <message>
        <location filename="../Banks/BankBy.cpp" line="96"/>
        <source>Belarussian Ruble (BYN)</source>
        <translation>Белорусский Рубль (BYN)</translation>
    </message>
    <message>
        <location filename="../Banks/BankRu.cpp" line="23"/>
        <source>Central Bank of the Russian Federation</source>
        <translation>Центральный банк Российской Федерации</translation>
    </message>
    <message>
        <location filename="../Banks/BankRu.cpp" line="113"/>
        <source>Russian Ruble (RUB)</source>
        <translation>Русский Рубль (RUB)</translation>
    </message>
    <message>
        <location filename="../Banks/BankUa.cpp" line="22"/>
        <source>National Bank of Ukraine</source>
        <translation>Национальный Банк Украины</translation>
    </message>
    <message>
        <location filename="../Banks/BankUa.cpp" line="116"/>
        <source>Ukrainian Hryvnia (UAH)</source>
        <translation>Украинская Гривна (AUH)</translation>
    </message>
    <message>
        <location filename="../TranslateCodeCurrency.cpp" line="7"/>
        <source>UAE Dirham</source>
        <translation>Дирхам ОАЭ</translation>
    </message>
    <message>
        <location filename="../TranslateCodeCurrency.cpp" line="8"/>
        <location filename="../TranslateCodeCurrency.cpp" line="9"/>
        <source>Afghani</source>
        <translation>Афгани</translation>
    </message>
    <message>
        <location filename="../TranslateCodeCurrency.cpp" line="10"/>
        <source>Albanian Lek</source>
        <translation>Албанский лек</translation>
    </message>
    <message>
        <location filename="../TranslateCodeCurrency.cpp" line="11"/>
        <source>Armenian Dram</source>
        <translation>Армянских драмов</translation>
    </message>
    <message>
        <location filename="../TranslateCodeCurrency.cpp" line="12"/>
        <source>Angolan new Kwanza</source>
        <translation>Ангольская новая кванза</translation>
    </message>
    <message>
        <location filename="../TranslateCodeCurrency.cpp" line="13"/>
        <source>Argentine Peso</source>
        <translation>Аргентинское песо</translation>
    </message>
    <message>
        <location filename="../TranslateCodeCurrency.cpp" line="14"/>
        <source>Austrian Schilling</source>
        <translation>Австрийский шиллинг</translation>
    </message>
    <message>
        <location filename="../TranslateCodeCurrency.cpp" line="15"/>
        <source>Australian Dollar</source>
        <translation>Австралийский доллар</translation>
    </message>
    <message>
        <location filename="../TranslateCodeCurrency.cpp" line="16"/>
        <location filename="../TranslateCodeCurrency.cpp" line="17"/>
        <source>Azerbaijanian Manat</source>
        <translation>Азербайджанский манат</translation>
    </message>
    <message>
        <location filename="../TranslateCodeCurrency.cpp" line="18"/>
        <source>Така</source>
        <translation>Така</translation>
    </message>
    <message>
        <location filename="../TranslateCodeCurrency.cpp" line="19"/>
        <source>Belgian Franc</source>
        <translation>Бельгийский франк</translation>
    </message>
    <message>
        <location filename="../TranslateCodeCurrency.cpp" line="20"/>
        <source>Bulgarian Lev</source>
        <translation>Болгарский лев</translation>
    </message>
    <message>
        <location filename="../TranslateCodeCurrency.cpp" line="21"/>
        <source>Bulgarian lev</source>
        <translation>Болгарский лев</translation>
    </message>
    <message>
        <location filename="../TranslateCodeCurrency.cpp" line="22"/>
        <source>Brazilian Real</source>
        <translation>Бразильский реал</translation>
    </message>
    <message>
        <location filename="../TranslateCodeCurrency.cpp" line="23"/>
        <source>Belarussian Ruble</source>
        <translation>Белорусский рубль</translation>
    </message>
    <message>
        <location filename="../TranslateCodeCurrency.cpp" line="24"/>
        <source>Canadian Dollar</source>
        <translation>Канадский доллар</translation>
    </message>
    <message>
        <location filename="../TranslateCodeCurrency.cpp" line="25"/>
        <source>Swiss Franc</source>
        <translation>Швейцарский франк</translation>
    </message>
    <message>
        <location filename="../TranslateCodeCurrency.cpp" line="26"/>
        <source>Chilean Peso</source>
        <translation>Чилийское песо</translation>
    </message>
    <message>
        <location filename="../TranslateCodeCurrency.cpp" line="27"/>
        <source>China Yuan</source>
        <translation>Китайский юань</translation>
    </message>
    <message>
        <location filename="../TranslateCodeCurrency.cpp" line="28"/>
        <source>Colombian Peso</source>
        <translation>Колумбийское песо</translation>
    </message>
    <message>
        <location filename="../TranslateCodeCurrency.cpp" line="29"/>
        <source>Cyprus Pound</source>
        <translation>Кипрский фунт</translation>
    </message>
    <message>
        <location filename="../TranslateCodeCurrency.cpp" line="30"/>
        <source>Czech Koruna</source>
        <translation>Чешский крон</translation>
    </message>
    <message>
        <location filename="../TranslateCodeCurrency.cpp" line="31"/>
        <source>Deutsche Mark</source>
        <translation>Немецкая марка</translation>
    </message>
    <message>
        <location filename="../TranslateCodeCurrency.cpp" line="32"/>
        <source>Danish Krone</source>
        <translation>Датских крон</translation>
    </message>
    <message>
        <location filename="../TranslateCodeCurrency.cpp" line="33"/>
        <source>Algerian Dinar</source>
        <translation>Алжирский динар</translation>
    </message>
    <message>
        <location filename="../TranslateCodeCurrency.cpp" line="34"/>
        <source>Estonian Crona</source>
        <translation>Эстонская крона</translation>
    </message>
    <message>
        <location filename="../TranslateCodeCurrency.cpp" line="35"/>
        <source>Egyptian Pound</source>
        <translation>Египетский фунт</translation>
    </message>
    <message>
        <location filename="../TranslateCodeCurrency.cpp" line="36"/>
        <source>Spanish Peseta</source>
        <translation>Испанская песета</translation>
    </message>
    <message>
        <location filename="../TranslateCodeCurrency.cpp" line="37"/>
        <source>Ethiopian Birr</source>
        <translation>Эфиопский быр</translation>
    </message>
    <message>
        <location filename="../TranslateCodeCurrency.cpp" line="38"/>
        <source>Euro</source>
        <translation>Евро</translation>
    </message>
    <message>
        <location filename="../TranslateCodeCurrency.cpp" line="39"/>
        <source>Finnish Marka</source>
        <translation>Финляндская марка</translation>
    </message>
    <message>
        <location filename="../TranslateCodeCurrency.cpp" line="40"/>
        <source>French Franc</source>
        <translation>Французский франк</translation>
    </message>
    <message>
        <location filename="../TranslateCodeCurrency.cpp" line="41"/>
        <source>British Pound Sterling</source>
        <translation>Фунт стерлингов Соединенного королевства</translation>
    </message>
    <message>
        <location filename="../TranslateCodeCurrency.cpp" line="42"/>
        <source>Georgian coupon</source>
        <translation>Грузинский купон</translation>
    </message>
    <message>
        <location filename="../TranslateCodeCurrency.cpp" line="43"/>
        <source>Georgian Lari</source>
        <translation>Грузинский лари</translation>
    </message>
    <message>
        <location filename="../TranslateCodeCurrency.cpp" line="44"/>
        <source>Greek Drachma</source>
        <translation>Греческая драхма</translation>
    </message>
    <message>
        <location filename="../TranslateCodeCurrency.cpp" line="45"/>
        <source>Hong Kong Dollar</source>
        <translation>Гонконгских долларов</translation>
    </message>
    <message>
        <location filename="../TranslateCodeCurrency.cpp" line="46"/>
        <source>Croatian Kuna</source>
        <translation>Хорватская куна</translation>
    </message>
    <message>
        <location filename="../TranslateCodeCurrency.cpp" line="47"/>
        <source>Hungarian Forint</source>
        <translation>Венгерских форинтов</translation>
    </message>
    <message>
        <location filename="../TranslateCodeCurrency.cpp" line="48"/>
        <source>Rupiah</source>
        <translation>Индонезийская рупия</translation>
    </message>
    <message>
        <location filename="../TranslateCodeCurrency.cpp" line="49"/>
        <source>Irish Pound</source>
        <translation>Ирландский фунт</translation>
    </message>
    <message>
        <location filename="../TranslateCodeCurrency.cpp" line="50"/>
        <source>New Israeli Sheqel</source>
        <translation>Новый израильский шекель</translation>
    </message>
    <message>
        <location filename="../TranslateCodeCurrency.cpp" line="51"/>
        <source>Indian Rupee</source>
        <translation>Индийский рупий</translation>
    </message>
    <message>
        <location filename="../TranslateCodeCurrency.cpp" line="52"/>
        <source>Iraqi Dinar</source>
        <translation>Иракcкий динар</translation>
    </message>
    <message>
        <location filename="../TranslateCodeCurrency.cpp" line="53"/>
        <source>Iranian Rial</source>
        <translation>Иранский риал</translation>
    </message>
    <message>
        <location filename="../TranslateCodeCurrency.cpp" line="54"/>
        <source>Iceland Krona</source>
        <translation>Исландская крона</translation>
    </message>
    <message>
        <location filename="../TranslateCodeCurrency.cpp" line="55"/>
        <source>Italian Lira</source>
        <translation>Итальянская лира</translation>
    </message>
    <message>
        <location filename="../TranslateCodeCurrency.cpp" line="56"/>
        <source>Jordanian Dinar</source>
        <translation>Иорданский динар</translation>
    </message>
    <message>
        <location filename="../TranslateCodeCurrency.cpp" line="57"/>
        <source>Japanese Yen</source>
        <translation>Японская  иена</translation>
    </message>
    <message>
        <location filename="../TranslateCodeCurrency.cpp" line="58"/>
        <source>Kenyan Shilling</source>
        <translation>Кенийский шиллинг</translation>
    </message>
    <message>
        <location filename="../TranslateCodeCurrency.cpp" line="59"/>
        <source>Kyrgyzstan Som</source>
        <translation>Киргизский сом</translation>
    </message>
    <message>
        <location filename="../TranslateCodeCurrency.cpp" line="60"/>
        <source>South Korean Won</source>
        <translation>Вон Республики Корея</translation>
    </message>
    <message>
        <location filename="../TranslateCodeCurrency.cpp" line="61"/>
        <source>Kuwaiti Dinar</source>
        <translation>Кувейтский динар</translation>
    </message>
    <message>
        <location filename="../TranslateCodeCurrency.cpp" line="62"/>
        <source>Kazakhstan Tenge</source>
        <translation>Казахстанский тенге</translation>
    </message>
    <message>
        <location filename="../TranslateCodeCurrency.cpp" line="63"/>
        <source>Lao Kip</source>
        <translation>Лаосский кип</translation>
    </message>
    <message>
        <location filename="../TranslateCodeCurrency.cpp" line="64"/>
        <source>Lebanese Pound</source>
        <translation>Ливанский фунт</translation>
    </message>
    <message>
        <location filename="../TranslateCodeCurrency.cpp" line="65"/>
        <source>Sri Lankan Rupee</source>
        <translation>Шри-ланкийская рупия</translation>
    </message>
    <message>
        <source>Sri Lanka Rupee</source>
        <translation type="vanished">Шри-ланкийская рупия</translation>
    </message>
    <message>
        <location filename="../TranslateCodeCurrency.cpp" line="66"/>
        <source>Lithuanian Lit</source>
        <translation>Литовский лит</translation>
    </message>
    <message>
        <location filename="../TranslateCodeCurrency.cpp" line="67"/>
        <source>Luxembourg Franc</source>
        <translation>Люксембургский франк</translation>
    </message>
    <message>
        <location filename="../TranslateCodeCurrency.cpp" line="68"/>
        <source>Latvian Lat</source>
        <translation>Латвийский лат</translation>
    </message>
    <message>
        <location filename="../TranslateCodeCurrency.cpp" line="69"/>
        <source>Libyan Dinar</source>
        <translation>Ливийcкий динар</translation>
    </message>
    <message>
        <location filename="../TranslateCodeCurrency.cpp" line="70"/>
        <source>Moroccan Diram</source>
        <translation>Марокканский дирхам</translation>
    </message>
    <message>
        <location filename="../TranslateCodeCurrency.cpp" line="71"/>
        <source>Moldova Lei</source>
        <translation>молдавский лей</translation>
    </message>
    <message>
        <location filename="../TranslateCodeCurrency.cpp" line="72"/>
        <source>Denar</source>
        <translation>Денар</translation>
    </message>
    <message>
        <location filename="../TranslateCodeCurrency.cpp" line="73"/>
        <source>Tugrik</source>
        <translation>Монгольский тугрик</translation>
    </message>
    <message>
        <location filename="../TranslateCodeCurrency.cpp" line="74"/>
        <source>Mexican Nuevo Peso</source>
        <translation>Мексиканское новое песо</translation>
    </message>
    <message>
        <location filename="../TranslateCodeCurrency.cpp" line="75"/>
        <source>Malaysian Ringgit</source>
        <translation>Малайзийский ринггит</translation>
    </message>
    <message>
        <location filename="../TranslateCodeCurrency.cpp" line="76"/>
        <source>Naira</source>
        <translation>Нигерийская найра</translation>
    </message>
    <message>
        <location filename="../TranslateCodeCurrency.cpp" line="77"/>
        <source>Netherlands Guilder</source>
        <translation>Нидерландский гульден</translation>
    </message>
    <message>
        <location filename="../TranslateCodeCurrency.cpp" line="78"/>
        <source>Norwegian Krone</source>
        <translation>Норвежский крон</translation>
    </message>
    <message>
        <location filename="../TranslateCodeCurrency.cpp" line="79"/>
        <source>Nepalese Rupee</source>
        <translation>Непальcкая рупия</translation>
    </message>
    <message>
        <location filename="../TranslateCodeCurrency.cpp" line="80"/>
        <source>New Zealand Dollar</source>
        <translation>Новозеландский доллар</translation>
    </message>
    <message>
        <location filename="../TranslateCodeCurrency.cpp" line="81"/>
        <source>Nuevo Sol</source>
        <translation>Новый соль</translation>
    </message>
    <message>
        <location filename="../TranslateCodeCurrency.cpp" line="82"/>
        <source>Philippine Peso</source>
        <translation>Филиппинcкое песо</translation>
    </message>
    <message>
        <location filename="../TranslateCodeCurrency.cpp" line="83"/>
        <source>Pakistan Rupee</source>
        <translation>Пакистанская рупия</translation>
    </message>
    <message>
        <location filename="../TranslateCodeCurrency.cpp" line="84"/>
        <location filename="../TranslateCodeCurrency.cpp" line="85"/>
        <source>Polish Zloty</source>
        <translation>Польский злотый</translation>
    </message>
    <message>
        <location filename="../TranslateCodeCurrency.cpp" line="86"/>
        <source>Portuguese Escudo</source>
        <translation>Партугальскае эскуда</translation>
    </message>
    <message>
        <location filename="../TranslateCodeCurrency.cpp" line="87"/>
        <source>Romanian Leu</source>
        <translation>Новый румынский лей</translation>
    </message>
    <message>
        <location filename="../TranslateCodeCurrency.cpp" line="88"/>
        <source>New romanian Leu</source>
        <translation>Румынский лей</translation>
    </message>
    <message>
        <location filename="../TranslateCodeCurrency.cpp" line="89"/>
        <source>Serbian Dinar</source>
        <translation>Сербский динар</translation>
    </message>
    <message>
        <location filename="../TranslateCodeCurrency.cpp" line="90"/>
        <location filename="../TranslateCodeCurrency.cpp" line="91"/>
        <source>Russian Ruble</source>
        <translation>Российский рубль</translation>
    </message>
    <message>
        <location filename="../TranslateCodeCurrency.cpp" line="92"/>
        <source>Saudi Riyal</source>
        <translation>Саудовский риял</translation>
    </message>
    <message>
        <location filename="../TranslateCodeCurrency.cpp" line="93"/>
        <source>Sudanese Dinar</source>
        <translation>Суданский динар</translation>
    </message>
    <message>
        <location filename="../TranslateCodeCurrency.cpp" line="94"/>
        <source>Sudanese Pound</source>
        <translation>Суданский фунт</translation>
    </message>
    <message>
        <location filename="../TranslateCodeCurrency.cpp" line="95"/>
        <source>Swedish Krona</source>
        <translation>Шведская крона</translation>
    </message>
    <message>
        <location filename="../TranslateCodeCurrency.cpp" line="96"/>
        <source>Singapore Dollar</source>
        <translation>Сингапурский доллар</translation>
    </message>
    <message>
        <location filename="../TranslateCodeCurrency.cpp" line="97"/>
        <source>Slovenian Tolar</source>
        <translation>Словенский толар</translation>
    </message>
    <message>
        <location filename="../TranslateCodeCurrency.cpp" line="98"/>
        <source>Slovak Koruna</source>
        <translation>Словацкая крона</translation>
    </message>
    <message>
        <location filename="../TranslateCodeCurrency.cpp" line="99"/>
        <source>Surinam Dollar</source>
        <translation>Суринамский доллар</translation>
    </message>
    <message>
        <location filename="../TranslateCodeCurrency.cpp" line="100"/>
        <source>Syrian Pound</source>
        <translation>Сирийский фунт</translation>
    </message>
    <message>
        <location filename="../TranslateCodeCurrency.cpp" line="101"/>
        <source>Baht</source>
        <translation>Таиландский бат</translation>
    </message>
    <message>
        <location filename="../TranslateCodeCurrency.cpp" line="102"/>
        <location filename="../TranslateCodeCurrency.cpp" line="103"/>
        <source>Tajikistan Ruble</source>
        <translation>Таджикский рубль</translation>
    </message>
    <message>
        <location filename="../TranslateCodeCurrency.cpp" line="104"/>
        <source>Turkmenistan Manat</source>
        <translation>Туркменский манат</translation>
    </message>
    <message>
        <location filename="../TranslateCodeCurrency.cpp" line="105"/>
        <source>New Turkmenistan Manat</source>
        <translation>Новый туркменский манат</translation>
    </message>
    <message>
        <location filename="../TranslateCodeCurrency.cpp" line="106"/>
        <source>Tunisian Dinar</source>
        <translation>Тунисский динар</translation>
    </message>
    <message>
        <location filename="../TranslateCodeCurrency.cpp" line="107"/>
        <source>Turkish Lira</source>
        <translation>Турецкая лира</translation>
    </message>
    <message>
        <location filename="../TranslateCodeCurrency.cpp" line="108"/>
        <source>New Turkish Lira</source>
        <translation>Турецкая лира</translation>
    </message>
    <message>
        <location filename="../TranslateCodeCurrency.cpp" line="109"/>
        <source>New Taiwan Dollar</source>
        <translation>Новый тайваньский доллар</translation>
    </message>
    <message>
        <location filename="../TranslateCodeCurrency.cpp" line="110"/>
        <source>Ukrainian Hryvnia</source>
        <translation>Украинская гривна</translation>
    </message>
    <message>
        <location filename="../TranslateCodeCurrency.cpp" line="111"/>
        <source>Karbovanet</source>
        <translation>Украинский карбованец</translation>
    </message>
    <message>
        <location filename="../TranslateCodeCurrency.cpp" line="112"/>
        <source>US Dollar</source>
        <translation>Доллар США</translation>
    </message>
    <message>
        <location filename="../TranslateCodeCurrency.cpp" line="113"/>
        <source>Peso Uruguayo</source>
        <translation>Песо Уругвайское</translation>
    </message>
    <message>
        <location filename="../TranslateCodeCurrency.cpp" line="114"/>
        <source>Uzbekistan Sum</source>
        <translation>Узбекский сум</translation>
    </message>
    <message>
        <location filename="../TranslateCodeCurrency.cpp" line="115"/>
        <location filename="../TranslateCodeCurrency.cpp" line="116"/>
        <source>Bolivar Fuerte</source>
        <translation>Боливар фуэрте</translation>
    </message>
    <message>
        <location filename="../TranslateCodeCurrency.cpp" line="117"/>
        <source>Bolivar Soberano</source>
        <translation>Боливар соберано</translation>
    </message>
    <message>
        <location filename="../TranslateCodeCurrency.cpp" line="118"/>
        <source>Dong</source>
        <translation>Донг</translation>
    </message>
    <message>
        <location filename="../TranslateCodeCurrency.cpp" line="119"/>
        <source>Silver</source>
        <translation>Серебро</translation>
    </message>
    <message>
        <location filename="../TranslateCodeCurrency.cpp" line="120"/>
        <source>Gold</source>
        <translation>Золото</translation>
    </message>
    <message>
        <location filename="../TranslateCodeCurrency.cpp" line="121"/>
        <source>Palladium</source>
        <translation>Палладий</translation>
    </message>
    <message>
        <location filename="../TranslateCodeCurrency.cpp" line="122"/>
        <source>Platinum</source>
        <translation>Платина</translation>
    </message>
    <message>
        <location filename="../TranslateCodeCurrency.cpp" line="123"/>
        <source>SDR (Special Drawing Rights)</source>
        <translation>СДР (Специальные права заимствования)</translation>
    </message>
    <message>
        <location filename="../TranslateCodeCurrency.cpp" line="124"/>
        <source>ECU</source>
        <translation>ЭКЮ</translation>
    </message>
    <message>
        <location filename="../TranslateCodeCurrency.cpp" line="125"/>
        <source>Yugoslavian Dinar</source>
        <translation>Югославский Динар</translation>
    </message>
    <message>
        <location filename="../TranslateCodeCurrency.cpp" line="126"/>
        <source>S.African Rand</source>
        <translation>Южноафриканский рэнд</translation>
    </message>
</context>
</TS>
