<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="en_US">
<context>
    <name>FormAbout</name>
    <message>
        <location filename="../FormAbout.ui" line="131"/>
        <source>License: GPLv3+</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../FormAbout.ui" line="208"/>
        <source>Visit web site</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../FormAbout.ui" line="226"/>
        <source>Close</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../FormAbout.cpp" line="15"/>
        <source>About</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../FormAbout.cpp" line="18"/>
        <source>Currency Converter</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../FormAbout.cpp" line="19"/>
        <source>Version:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../FormAbout.cpp" line="20"/>
        <source>Date build:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../FormAbout.cpp" line="21"/>
        <source>All rights reserved.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>FormDenialResponsibility</name>
    <message>
        <location filename="../FormDenialResponsibility.ui" line="62"/>
        <source>Software are granted on an &quot;as is&quot; basis which means that the Rightholder does not provide to the User any guarantee that: currency converter will meet User&apos;s requirements,currency converter will be provided in a continuous, timely, reliable and error-free manner; results which can be received with their use will be accurate and reliable; all errors will be corrected.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../FormDenialResponsibility.ui" line="88"/>
        <source>Accept</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../FormDenialResponsibility.cpp" line="9"/>
        <source>Вenial of responsibility</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>FormVisibilityCurrency</name>
    <message>
        <location filename="../FormVisibilityCurrency.ui" line="20"/>
        <source>Visibility of the currency</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../FormVisibilityCurrency.ui" line="48"/>
        <source>Code</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../FormVisibilityCurrency.ui" line="53"/>
        <source>Name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../FormVisibilityCurrency.ui" line="74"/>
        <source>Accept</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../FormVisibilityCurrency.ui" line="81"/>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../FormVisibilityCurrency.cpp" line="15"/>
        <source>Visibility currency</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../MainWindow.ui" line="124"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;a href=&quot;http://dansoft.ru&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0000ff;&quot;&gt;© 2019 DanSoft. All rights reserved&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="28"/>
        <source>Banks:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="110"/>
        <source>Get courses</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="291"/>
        <source>File</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="297"/>
        <source>Setup</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="303"/>
        <location filename="../MainWindow.ui" line="314"/>
        <source>About</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="319"/>
        <source>Exit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="324"/>
        <source>Visibility of the currency</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="329"/>
        <source>Denial of responsibility</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="27"/>
        <source>Currency converter v%1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="161"/>
        <source>Date</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="162"/>
        <source>Nominal</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="163"/>
        <source>Rate</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="196"/>
        <source>Loading courses</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="196"/>
        <source>Exchange rates are being downloaded. Please wait...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="241"/>
        <source>express converter at the rate of</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../Banks/BankBy.cpp" line="24"/>
        <source>National Bank of the Republic of Belarus</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Banks/BankBy.cpp" line="40"/>
        <location filename="../Banks/BankRu.cpp" line="39"/>
        <location filename="../Banks/BankUa.cpp" line="39"/>
        <source>Currency converter</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Banks/BankBy.cpp" line="40"/>
        <location filename="../Banks/BankRu.cpp" line="39"/>
        <location filename="../Banks/BankUa.cpp" line="39"/>
        <source>is not available</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Banks/BankBy.cpp" line="74"/>
        <location filename="../Banks/BankRu.cpp" line="88"/>
        <location filename="../Banks/BankUa.cpp" line="91"/>
        <source>Currency Converter</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Banks/BankBy.cpp" line="74"/>
        <location filename="../Banks/BankRu.cpp" line="88"/>
        <location filename="../Banks/BankUa.cpp" line="91"/>
        <source>There are no courses for this number. The Bank offers to use %1 exchange rates</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Banks/BankBy.cpp" line="96"/>
        <source>Belarussian Ruble (BYN)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Banks/BankRu.cpp" line="23"/>
        <source>Central Bank of the Russian Federation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Banks/BankRu.cpp" line="113"/>
        <source>Russian Ruble (RUB)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Banks/BankUa.cpp" line="22"/>
        <source>National Bank of Ukraine</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Banks/BankUa.cpp" line="116"/>
        <source>Ukrainian Hryvnia (UAH)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../TranslateCodeCurrency.cpp" line="7"/>
        <source>UAE Dirham</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../TranslateCodeCurrency.cpp" line="8"/>
        <location filename="../TranslateCodeCurrency.cpp" line="9"/>
        <source>Afghani</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../TranslateCodeCurrency.cpp" line="10"/>
        <source>Albanian Lek</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../TranslateCodeCurrency.cpp" line="11"/>
        <source>Armenian Dram</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../TranslateCodeCurrency.cpp" line="12"/>
        <source>Angolan new Kwanza</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../TranslateCodeCurrency.cpp" line="13"/>
        <source>Argentine Peso</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../TranslateCodeCurrency.cpp" line="14"/>
        <source>Austrian Schilling</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../TranslateCodeCurrency.cpp" line="15"/>
        <source>Australian Dollar</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../TranslateCodeCurrency.cpp" line="16"/>
        <location filename="../TranslateCodeCurrency.cpp" line="17"/>
        <source>Azerbaijanian Manat</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../TranslateCodeCurrency.cpp" line="18"/>
        <source>Така</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../TranslateCodeCurrency.cpp" line="19"/>
        <source>Belgian Franc</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../TranslateCodeCurrency.cpp" line="20"/>
        <source>Bulgarian Lev</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../TranslateCodeCurrency.cpp" line="21"/>
        <source>Bulgarian lev</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../TranslateCodeCurrency.cpp" line="22"/>
        <source>Brazilian Real</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../TranslateCodeCurrency.cpp" line="23"/>
        <source>Belarussian Ruble</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../TranslateCodeCurrency.cpp" line="24"/>
        <source>Canadian Dollar</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../TranslateCodeCurrency.cpp" line="25"/>
        <source>Swiss Franc</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../TranslateCodeCurrency.cpp" line="26"/>
        <source>Chilean Peso</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../TranslateCodeCurrency.cpp" line="27"/>
        <source>China Yuan</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../TranslateCodeCurrency.cpp" line="28"/>
        <source>Colombian Peso</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../TranslateCodeCurrency.cpp" line="29"/>
        <source>Cyprus Pound</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../TranslateCodeCurrency.cpp" line="30"/>
        <source>Czech Koruna</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../TranslateCodeCurrency.cpp" line="31"/>
        <source>Deutsche Mark</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../TranslateCodeCurrency.cpp" line="32"/>
        <source>Danish Krone</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../TranslateCodeCurrency.cpp" line="33"/>
        <source>Algerian Dinar</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../TranslateCodeCurrency.cpp" line="34"/>
        <source>Estonian Crona</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../TranslateCodeCurrency.cpp" line="35"/>
        <source>Egyptian Pound</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../TranslateCodeCurrency.cpp" line="36"/>
        <source>Spanish Peseta</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../TranslateCodeCurrency.cpp" line="37"/>
        <source>Ethiopian Birr</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../TranslateCodeCurrency.cpp" line="38"/>
        <source>Euro</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../TranslateCodeCurrency.cpp" line="39"/>
        <source>Finnish Marka</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../TranslateCodeCurrency.cpp" line="40"/>
        <source>French Franc</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../TranslateCodeCurrency.cpp" line="41"/>
        <source>British Pound Sterling</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../TranslateCodeCurrency.cpp" line="42"/>
        <source>Georgian coupon</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../TranslateCodeCurrency.cpp" line="43"/>
        <source>Georgian Lari</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../TranslateCodeCurrency.cpp" line="44"/>
        <source>Greek Drachma</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../TranslateCodeCurrency.cpp" line="45"/>
        <source>Hong Kong Dollar</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../TranslateCodeCurrency.cpp" line="46"/>
        <source>Croatian Kuna</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../TranslateCodeCurrency.cpp" line="47"/>
        <source>Hungarian Forint</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../TranslateCodeCurrency.cpp" line="48"/>
        <source>Rupiah</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../TranslateCodeCurrency.cpp" line="49"/>
        <source>Irish Pound</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../TranslateCodeCurrency.cpp" line="50"/>
        <source>New Israeli Sheqel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../TranslateCodeCurrency.cpp" line="51"/>
        <source>Indian Rupee</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../TranslateCodeCurrency.cpp" line="52"/>
        <source>Iraqi Dinar</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../TranslateCodeCurrency.cpp" line="53"/>
        <source>Iranian Rial</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../TranslateCodeCurrency.cpp" line="54"/>
        <source>Iceland Krona</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../TranslateCodeCurrency.cpp" line="55"/>
        <source>Italian Lira</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../TranslateCodeCurrency.cpp" line="56"/>
        <source>Jordanian Dinar</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../TranslateCodeCurrency.cpp" line="57"/>
        <source>Japanese Yen</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../TranslateCodeCurrency.cpp" line="58"/>
        <source>Kenyan Shilling</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../TranslateCodeCurrency.cpp" line="59"/>
        <source>Kyrgyzstan Som</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../TranslateCodeCurrency.cpp" line="60"/>
        <source>South Korean Won</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../TranslateCodeCurrency.cpp" line="61"/>
        <source>Kuwaiti Dinar</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../TranslateCodeCurrency.cpp" line="62"/>
        <source>Kazakhstan Tenge</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../TranslateCodeCurrency.cpp" line="63"/>
        <source>Lao Kip</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../TranslateCodeCurrency.cpp" line="64"/>
        <source>Lebanese Pound</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../TranslateCodeCurrency.cpp" line="65"/>
        <source>Sri Lankan Rupee</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../TranslateCodeCurrency.cpp" line="66"/>
        <source>Lithuanian Lit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../TranslateCodeCurrency.cpp" line="67"/>
        <source>Luxembourg Franc</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../TranslateCodeCurrency.cpp" line="68"/>
        <source>Latvian Lat</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../TranslateCodeCurrency.cpp" line="69"/>
        <source>Libyan Dinar</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../TranslateCodeCurrency.cpp" line="70"/>
        <source>Moroccan Diram</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../TranslateCodeCurrency.cpp" line="71"/>
        <source>Moldova Lei</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../TranslateCodeCurrency.cpp" line="72"/>
        <source>Denar</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../TranslateCodeCurrency.cpp" line="73"/>
        <source>Tugrik</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../TranslateCodeCurrency.cpp" line="74"/>
        <source>Mexican Nuevo Peso</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../TranslateCodeCurrency.cpp" line="75"/>
        <source>Malaysian Ringgit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../TranslateCodeCurrency.cpp" line="76"/>
        <source>Naira</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../TranslateCodeCurrency.cpp" line="77"/>
        <source>Netherlands Guilder</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../TranslateCodeCurrency.cpp" line="78"/>
        <source>Norwegian Krone</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../TranslateCodeCurrency.cpp" line="79"/>
        <source>Nepalese Rupee</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../TranslateCodeCurrency.cpp" line="80"/>
        <source>New Zealand Dollar</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../TranslateCodeCurrency.cpp" line="81"/>
        <source>Nuevo Sol</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../TranslateCodeCurrency.cpp" line="82"/>
        <source>Philippine Peso</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../TranslateCodeCurrency.cpp" line="83"/>
        <source>Pakistan Rupee</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../TranslateCodeCurrency.cpp" line="84"/>
        <location filename="../TranslateCodeCurrency.cpp" line="85"/>
        <source>Polish Zloty</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../TranslateCodeCurrency.cpp" line="86"/>
        <source>Portuguese Escudo</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../TranslateCodeCurrency.cpp" line="87"/>
        <source>Romanian Leu</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../TranslateCodeCurrency.cpp" line="88"/>
        <source>New romanian Leu</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../TranslateCodeCurrency.cpp" line="89"/>
        <source>Serbian Dinar</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../TranslateCodeCurrency.cpp" line="90"/>
        <location filename="../TranslateCodeCurrency.cpp" line="91"/>
        <source>Russian Ruble</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../TranslateCodeCurrency.cpp" line="92"/>
        <source>Saudi Riyal</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../TranslateCodeCurrency.cpp" line="93"/>
        <source>Sudanese Dinar</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../TranslateCodeCurrency.cpp" line="94"/>
        <source>Sudanese Pound</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../TranslateCodeCurrency.cpp" line="95"/>
        <source>Swedish Krona</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../TranslateCodeCurrency.cpp" line="96"/>
        <source>Singapore Dollar</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../TranslateCodeCurrency.cpp" line="97"/>
        <source>Slovenian Tolar</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../TranslateCodeCurrency.cpp" line="98"/>
        <source>Slovak Koruna</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../TranslateCodeCurrency.cpp" line="99"/>
        <source>Surinam Dollar</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../TranslateCodeCurrency.cpp" line="100"/>
        <source>Syrian Pound</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../TranslateCodeCurrency.cpp" line="101"/>
        <source>Baht</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../TranslateCodeCurrency.cpp" line="102"/>
        <location filename="../TranslateCodeCurrency.cpp" line="103"/>
        <source>Tajikistan Ruble</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../TranslateCodeCurrency.cpp" line="104"/>
        <source>Turkmenistan Manat</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../TranslateCodeCurrency.cpp" line="105"/>
        <source>New Turkmenistan Manat</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../TranslateCodeCurrency.cpp" line="106"/>
        <source>Tunisian Dinar</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../TranslateCodeCurrency.cpp" line="107"/>
        <source>Turkish Lira</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../TranslateCodeCurrency.cpp" line="108"/>
        <source>New Turkish Lira</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../TranslateCodeCurrency.cpp" line="109"/>
        <source>New Taiwan Dollar</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../TranslateCodeCurrency.cpp" line="110"/>
        <source>Ukrainian Hryvnia</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../TranslateCodeCurrency.cpp" line="111"/>
        <source>Karbovanet</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../TranslateCodeCurrency.cpp" line="112"/>
        <source>US Dollar</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../TranslateCodeCurrency.cpp" line="113"/>
        <source>Peso Uruguayo</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../TranslateCodeCurrency.cpp" line="114"/>
        <source>Uzbekistan Sum</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../TranslateCodeCurrency.cpp" line="115"/>
        <location filename="../TranslateCodeCurrency.cpp" line="116"/>
        <source>Bolivar Fuerte</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../TranslateCodeCurrency.cpp" line="117"/>
        <source>Bolivar Soberano</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../TranslateCodeCurrency.cpp" line="118"/>
        <source>Dong</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../TranslateCodeCurrency.cpp" line="119"/>
        <source>Silver</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../TranslateCodeCurrency.cpp" line="120"/>
        <source>Gold</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../TranslateCodeCurrency.cpp" line="121"/>
        <source>Palladium</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../TranslateCodeCurrency.cpp" line="122"/>
        <source>Platinum</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../TranslateCodeCurrency.cpp" line="123"/>
        <source>SDR (Special Drawing Rights)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../TranslateCodeCurrency.cpp" line="124"/>
        <source>ECU</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../TranslateCodeCurrency.cpp" line="125"/>
        <source>Yugoslavian Dinar</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../TranslateCodeCurrency.cpp" line="126"/>
        <source>S.African Rand</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
