#include "FormVisibilityCurrency.h"
#include "ui_FormVisibilityCurrency.h"

#include <QDir>
#include <QStandardPaths>
#include <QTableWidgetItem>

#include "TranslateCodeCurrency.h"

FormVisibilityCurrency::FormVisibilityCurrency(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::FormVisibilityCurrency)
{
    ui->setupUi(this);
    this->setWindowTitle(tr("Visibility currency"));


    confSettings = new QSettings (QStandardPaths::writableLocation(QStandardPaths::AppConfigLocation)+"/settings.ini", QSettings::IniFormat);
    confSettings->setPath(QSettings::IniFormat, QSettings::UserScope, QDir::currentPath());


    TranslateCodeCurrency trCodeCurrency;

    QStringList listCodes = trCodeCurrency.getAllCodes();

    ui->tableWidget->setRowCount(listCodes.size());

    ui->tableWidget->horizontalHeader()->setSectionResizeMode(0,QHeaderView::ResizeToContents);
    ui->tableWidget->horizontalHeader()->setSectionResizeMode(1,QHeaderView::ResizeToContents);
    ui->tableWidget->horizontalHeader()->setSectionResizeMode(2,QHeaderView::Stretch);

    QTableWidgetItem* temp;
    int row=0;

    for (auto &x:listCodes) {
        temp = new QTableWidgetItem();
        if (confSettings->value("visibility_codes/"+x,"true").toString()=="true"){
            temp->setCheckState(Qt::Checked);
        }else{
            temp->setCheckState(Qt::Unchecked);
        }
        ui->tableWidget->setItem(row,0,temp);

        temp = new QTableWidgetItem();
        temp->setText(x);
        ui->tableWidget->setItem(row,1,temp);

        temp = new QTableWidgetItem();
        temp->setText(trCodeCurrency.CodeToString(x));
        ui->tableWidget->setItem(row,2,temp);

        row++;
    }


    connect(ui->pushButton,&QAbstractButton::clicked,this,&FormVisibilityCurrency::close);
    connect(ui->pushButton_2,&QAbstractButton::clicked,this,&FormVisibilityCurrency::saveAndClose);

}

FormVisibilityCurrency::~FormVisibilityCurrency(){
    delete ui;
}

void FormVisibilityCurrency::saveAndClose(){
    for (int i=0;i<ui->tableWidget->rowCount();i++){
        QString code = ui->tableWidget->item(i,1)->text();
        QString visible = ui->tableWidget->item(i,0)->checkState()==Qt::Checked ? "true":"false";

        confSettings->setValue("visibility_codes/"+code,visible);
    }


    accept();
}
