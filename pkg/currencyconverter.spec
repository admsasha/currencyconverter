Summary:	Currency converter
Name:		currencyconverter
Version:	1.0.0
Release:	1
License:	GPLv3+
Group:		Office
Url:		https://bitbucket.org/admsasha/currencyconverter
Source0:	https://bitbucket.org/admsasha/currencyconverter/downloads/%{name}-%{version}.tar.gz

BuildRequires:	qt5-linguist-tools
BuildRequires:	pkgconfig(Qt5Core)
BuildRequires:	pkgconfig(Qt5Gui)
BuildRequires:	pkgconfig(Qt5Widgets)
BuildRequires:	pkgconfig(Qt5Network)
BuildRequires:	pkgconfig(Qt5Sql)
BuildRequires:	pkgconfig(Qt5Xml)

%description
Converter currency exchange rates

%files
%doc README.md
%{_bindir}/%{name}
%{_datadir}/applications/%{name}.desktop
%{_datadir}/%{name}/
%{_iconsdir}/hicolor/*/apps/%{name}.png

#----------------------------------------------------------------------------

%prep
%setup -q

%build
lrelease ./CurrencyConverter.pro
%qmake_qt5
%make

%install
%make_install INSTALL_ROOT=%{buildroot}
