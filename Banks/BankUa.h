#ifndef BANKUA_H
#define BANKUA_H

#include "IBank.h"

class BankUa : public IBank {
    public:
        BankUa();
        ~BankUa() override;

        QString name() override;
        bool getCourses(QDate date) override;
        QString currency() override;
};

#endif // BANKUA_H
