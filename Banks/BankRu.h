#ifndef BANKRU_H
#define BANKRU_H

#include "IBank.h"


class BankRu : public IBank {
    public:
        BankRu();
        ~BankRu() override;

        QString name() override;
        bool getCourses(QDate date) override;
        QString currency() override;
};

#endif // BANKRU_H
