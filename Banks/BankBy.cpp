#include "BankBy.h"

#include <QtDebug>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>
#include <QSqlDatabase>
#include <QSqlQuery>
#include <QSqlError>
#include <QMessageBox>

#include "DownloadManager.h"

BankBy::BankBy() : IBank("BankBy"){
    // https://www.nbrb.by/APIHelp/ExRates
}

BankBy::~BankBy(){

}

QString BankBy::name()
{
    return QObject::tr("National Bank of the Republic of Belarus");

}

bool BankBy::getCourses(QDate date){
    QSqlDatabase db = QSqlDatabase::database("currencyconverter");
    QSqlQuery query(QSqlDatabase::database("currencyconverter"));

    DownloadManager mng;


    QString date_str = date.toString("yyyy-MM-dd");
    QByteArray jsonData =mng.execute("http://www.nbrb.by/api/exrates/rates?ondate="+date_str+"&periodicity=0",false);
    if (mng.getErrorCode()!=QNetworkReply::NoError or mng.getStatusCode()!=200){
        qDebug() << mng.getErrorCode() << mng.getStatusCode();
        qDebug() << jsonData.size();
        QMessageBox::critical(nullptr,QObject::tr("Currency converter"),"www.nbrb.by "+QObject::tr("is not available"));
        return  false;
    }

    QVector<CURRENCY_EXCHANGE> listCurrencyExchange;
    QString dateCurrency="";

    QJsonDocument document = QJsonDocument::fromJson(jsonData);
    if (document.isEmpty()){
        return false;
    }

    QJsonArray arrJson = document.array();
    for (int i=0;i<arrJson.size();i++){
        CURRENCY_EXCHANGE currencyExchange;

        QString Cur_Abbreviation = arrJson.at(i).toObject().value("Cur_Abbreviation").toString();
        double Cur_OfficialRate = arrJson.at(i).toObject().value("Cur_OfficialRate").toDouble();
        int Cur_Scale = arrJson.at(i).toObject().value("Cur_Scale").toInt();
        QString Date = arrJson.at(i).toObject().value("Date").toString();

        currencyExchange.code=Cur_Abbreviation;
        currencyExchange.nominal=Cur_Scale;
        currencyExchange.rate=Cur_OfficialRate;

        listCurrencyExchange.push_back(currencyExchange);

        dateCurrency = Date.split("T").at(0);
    }


    if (dateCurrency.isEmpty()) return false;

    if (dateCurrency!=date.toString("yyyy-MM-dd")){
        QMessageBox::critical(nullptr,QObject::tr("Currency Converter"),QObject::tr("There are no courses for this number. The Bank offers to use %1 exchange rates").arg(dateCurrency));
        return false;
    }

    db.transaction();
    for (auto &x:listCurrencyExchange){
        query.prepare("INSERT INTO exchange_rate (`code_bank`,`date`,`currency_code`,`nominal`,`rate`) VALUES (:code_bank,:date,:currency_code,:nominal,:rate)");
        query.bindValue(":code_bank",getCodeBank());
        query.bindValue(":date",dateCurrency);
        query.bindValue(":currency_code",x.code);
        query.bindValue(":nominal",x.nominal);
        query.bindValue(":rate",x.rate);
        if(!query.exec()){
            qDebug() << query.lastError().text();
        }
    }
    db.commit();

    return true;
}

QString BankBy::currency(){
    return QObject::tr("Belarussian Ruble (BYN)");
}
