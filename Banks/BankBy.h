#ifndef BANKBY_H
#define BANKBY_H

#include "IBank.h"

class BankBy : public IBank {
    public:
        BankBy();
        ~BankBy() override;

        QString name() override;
        bool getCourses(QDate date) override;
        QString currency() override;
};

#endif // BANKBY_H
