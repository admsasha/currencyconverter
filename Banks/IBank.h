#ifndef IBANK_H
#define IBANK_H

#include <QObject>
#include <QString>
#include <QVector>
#include <QDate>

struct CURRENCY_EXCHANGE {
    QString code;
    int nominal;
    double rate;
    QString name;

    CURRENCY_EXCHANGE():nominal(1),rate(0.0){}
};

class IBank {
    public:
        explicit IBank(QString codeBank);
        virtual ~IBank();

        QString getCodeBank();

        virtual QString name() =0;
        virtual bool getCourses(QDate date) =0;
        virtual QString currency() =0;


    private:
        QString _codeBank;
};

#endif // IBANK_H
