#include "BankRu.h"

#include <QDomDocument>
#include <QtXml>
#include <QtDebug>
#include <QSqlDatabase>
#include <QSqlQuery>
#include <QSqlError>
#include <QMessageBox>

#include "DownloadManager.h"

BankRu::BankRu() : IBank("BankRu"){
    // https://cbr.ru/development/SXML/
}

BankRu::~BankRu(){

}

QString BankRu::name()
{
    return QObject::tr("Central Bank of the Russian Federation");

}

bool BankRu::getCourses(QDate date){
    QSqlDatabase db = QSqlDatabase::database("currencyconverter");
    QSqlQuery query(QSqlDatabase::database("currencyconverter"));

    DownloadManager mng;


    QString date_str = date.toString("dd/MM/yyyy");
    QString xmlData =mng.execute("http://www.cbr.ru/scripts/XML_daily.asp?date_req="+date_str,false);
    if (mng.getErrorCode()!=QNetworkReply::NoError or mng.getStatusCode()!=200){
        qDebug() << mng.getErrorCode() << mng.getStatusCode();
        qDebug() << xmlData.size();
        QMessageBox::critical(nullptr,QObject::tr("Currency converter"),"www.cbr.ru "+QObject::tr("is not available"));
        return  false;
    }

    QVector<CURRENCY_EXCHANGE> listCurrencyExchange;
    QString dateCurrency="";

    QDomDocument xml;
    if (!xml.setContent(xmlData)){
        return  false;
    }
    QDomNode n = xml.firstChild();
    while(!n.isNull()) {
        QDomElement e = n.toElement();

        if (e.tagName()=="ValCurs"){
            QStringList ValCursDates = e.attribute("Date").split(".");
            if (ValCursDates.size()==3){
                dateCurrency = QString("%1-%2-%3").arg(ValCursDates.at(2)).arg(ValCursDates.at(1).toInt(),2,10,QLatin1Char('0')).arg(ValCursDates.at(0).toInt(),2,10,QLatin1Char('0'));
            }


            QDomNode n2  = e.firstChild();
            while(!n2.isNull()) {
                QDomElement e2 = n2.toElement();
                if (e2.tagName()=="Valute"){
                    QDomNode n3  = e2.firstChild();
                    CURRENCY_EXCHANGE currencyExchange;
                    while(!n3.isNull()){
                        QDomElement e3 = n3.toElement();
                        if (e3.tagName()=="CharCode") currencyExchange.code=e3.text();
                        if (e3.tagName()=="Nominal") currencyExchange.nominal=e3.text().toInt();
                        if (e3.tagName()=="Value") currencyExchange.rate=e3.text().replace(",",".").toDouble();

                        n3 = n3.nextSibling();
                    }
                    listCurrencyExchange.push_back(currencyExchange);
                }
                n2 = n2.nextSibling();
            }
        }


        n = n.nextSibling();
    }

    if (dateCurrency.isEmpty()) return false;

    if (dateCurrency!=date.toString("yyyy-MM-dd")){
        QMessageBox::critical(nullptr,QObject::tr("Currency Converter"),QObject::tr("There are no courses for this number. The Bank offers to use %1 exchange rates").arg(dateCurrency));
        return false;
    }

    db.transaction();
    for (auto &x:listCurrencyExchange){
        query.prepare("INSERT INTO exchange_rate (`code_bank`,`date`,`currency_code`,`nominal`,`rate`) VALUES (:code_bank,:date,:currency_code,:nominal,:rate)");
        query.bindValue(":code_bank",getCodeBank());
        query.bindValue(":date",dateCurrency);
        query.bindValue(":currency_code",x.code);
        query.bindValue(":nominal",x.nominal);
        query.bindValue(":rate",x.rate);
        if(!query.exec()){
            qDebug() << query.lastError().text();
        }
    }
    db.commit();



    return true;
}


QString BankRu::currency(){
    return QObject::tr("Russian Ruble (RUB)");
}
