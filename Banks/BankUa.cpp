#include "BankUa.h"

#include <QDomDocument>
#include <QtXml>
#include <QtDebug>
#include <QSqlDatabase>
#include <QSqlQuery>
#include <QSqlError>
#include <QMessageBox>

#include "DownloadManager.h"

BankUa::BankUa() : IBank("BankUa"){
    // https://bank.gov.ua/control/uk/publish/article?art_id=38441973
}

BankUa::~BankUa(){

}

QString BankUa::name(){
    return QObject::tr("National Bank of Ukraine");

}


bool BankUa::getCourses(QDate date){
    QSqlDatabase db = QSqlDatabase::database("currencyconverter");
    QSqlQuery query(QSqlDatabase::database("currencyconverter"));

    DownloadManager mng;


    QString date_str = date.toString("yyyyMMdd");
    QString xmlData =mng.execute("https://bank.gov.ua/NBUStatService/v1/statdirectory/exchange?date="+date_str,false);
    if (mng.getErrorCode()!=QNetworkReply::NoError or mng.getStatusCode()!=200){
        qDebug() << mng.getErrorCode() << mng.getStatusCode();
        qDebug() << xmlData.size();
        QMessageBox::critical(nullptr,QObject::tr("Currency converter"),"bank.gov.ua "+QObject::tr("is not available"));
        return  false;
    }

    QVector<CURRENCY_EXCHANGE> listCurrencyExchange;
    QString dateCurrency="";

    QDomDocument xml;
    if (!xml.setContent(xmlData)){
        return  false;
    }
    QDomNode n = xml.firstChild();
    while(!n.isNull()) {
        QDomElement e = n.toElement();
        if (e.tagName()=="exchange"){

            QDomNode n2  = e.firstChild();
            while(!n2.isNull()) {
                QDomElement e2 = n2.toElement();
                if (e2.tagName()=="currency"){
                    QDomNode n3  = e2.firstChild();
                    CURRENCY_EXCHANGE currencyExchange;
                    while(!n3.isNull()){
                        QDomElement e3 = n3.toElement();
                        if (e3.tagName()=="cc") currencyExchange.code=e3.text();
                        if (e3.tagName()=="rate") currencyExchange.rate=e3.text().toDouble();

                        if (e3.tagName()=="exchangedate"){
                            QStringList pairData = e3.text().split(".");
                            if (pairData.size()==3){
                                dateCurrency=pairData.at(2)+"-"+pairData.at(1)+"-"+pairData.at(0);
                            }
                        }



                        n3 = n3.nextSibling();
                    }
                    listCurrencyExchange.push_back(currencyExchange);
                }
                n2 = n2.nextSibling();
            }
        }


        n = n.nextSibling();
    }


    if (dateCurrency.isEmpty()) return false;

    if (dateCurrency!=date.toString("yyyy-MM-dd")){
        QMessageBox::critical(nullptr,QObject::tr("Currency Converter"),QObject::tr("There are no courses for this number. The Bank offers to use %1 exchange rates").arg(dateCurrency));
        return false;
    }

    db.transaction();
    for (auto &x:listCurrencyExchange){
        query.prepare("INSERT INTO exchange_rate (`code_bank`,`date`,`currency_code`,`nominal`,`rate`) VALUES (:code_bank,:date,:currency_code,:nominal,:rate)");
        query.bindValue(":code_bank",getCodeBank());
        query.bindValue(":date",dateCurrency);
        query.bindValue(":currency_code",x.code);
        query.bindValue(":nominal",x.nominal);
        query.bindValue(":rate",x.rate);
        if(!query.exec()){
            qDebug() << query.lastError().text();
        }
    }
    db.commit();



    return true;
}


QString BankUa::currency(){
    return QObject::tr("Ukrainian Hryvnia (UAH)");
}
