#ifndef FORMDENIALRESPONSIBILITY_H
#define FORMDENIALRESPONSIBILITY_H

#include <QDialog>

namespace Ui {
class FormDenialResponsibility;
}

class FormDenialResponsibility : public QDialog
{
    Q_OBJECT

public:
    explicit FormDenialResponsibility(QWidget *parent = nullptr);
    ~FormDenialResponsibility();

private:
    Ui::FormDenialResponsibility *ui;
};

#endif // FORMDENIALRESPONSIBILITY_H
