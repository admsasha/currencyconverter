#include "DownloadManager.h"
#include <QNetworkAccessManager>
#include <QNetworkRequest>
#include <QNetworkReply>
#include <QUrl>
#include <QStringList>
#include <QDebug>

DownloadManager::DownloadManager(){
    connect(&manager, SIGNAL(finished(QNetworkReply*)),SLOT(downloadFinished(QNetworkReply*)));

    connect(&timer, SIGNAL(timeout()), this, SLOT(stop()));
    timer.setSingleShot(true);

    reply_busy=false;
    _timeout=0;
}

QByteArray DownloadManager::execute(QString pathurl,bool async,QString method,int timeout){
    QUrl url = QUrl::fromUserInput(pathurl);
    QNetworkRequest request(url);

    timer.stop();
    if (timeout>0){
        timer.start(timeout*1000);
    }else if (_timeout>0){
        timer.start(_timeout*1000);
    }

    url_data="";    
    reply_busy=true;
    errorCode=QNetworkReply::NoError;

    if (method=="get"){
        reply = manager.get(request);
    }else{
        if (pathurl.split("?").size()==2){
            request.setUrl(QUrl::fromUserInput(pathurl.split("?").at(0)));
            request.setHeader(QNetworkRequest::ContentTypeHeader, QVariant("application/x-www-form-urlencoded"));
            reply = manager.post(request,pathurl.split("?").at(1).toUtf8());
        }else{
            reply = manager.post(request,"");
        }
    }

    connect(reply,SIGNAL(downloadProgress(qint64,qint64)),this,SLOT(progress(qint64,qint64)));

    if (async==false) loop.exec();

    return url_data;
}

int DownloadManager::getStatusCode(){
    return statusCode.toInt();
}

QString DownloadManager::getContentTypeHeader(){
    return httpContentTypeHeader.toString();
}

QNetworkReply::NetworkError DownloadManager::getErrorCode(){
    return errorCode;
}

void DownloadManager::progress(qint64 value1, qint64 value2){
    emit downloadProgress(value1,value2);
}

void DownloadManager::downloadFinished(QNetworkReply *reply){
    QUrl url = reply->url();

    timer.stop();
    if (reply_busy==false) return;
    reply_busy=false;

    errorCode=reply->error();
    if (reply->error()!=QNetworkReply::NoError) {
        qDebug() << "reply" << url << "error " << reply->error();

        url_data=reply->errorString().toUtf8();

        statusCode = reply->attribute(QNetworkRequest::HttpStatusCodeAttribute);
        httpReasonPhrase = reply->attribute(QNetworkRequest::HttpReasonPhraseAttribute);
        httpContentTypeHeader = reply->header(QNetworkRequest::ContentTypeHeader);

        reply->deleteLater();
        emit downloadError(url,url_data);

    } else {
        //qDebug() << "reply" << url << "no eroror";

        statusCode = reply->attribute(QNetworkRequest::HttpStatusCodeAttribute);
        httpReasonPhrase = reply->attribute(QNetworkRequest::HttpReasonPhraseAttribute);
        httpContentTypeHeader = reply->header(QNetworkRequest::ContentTypeHeader);


        QByteArray bytes = reply->readAll();

        if (statusCode.toInt()==200){
            url_data=bytes;
        }else{
            url_data="";
        }
        reply->deleteLater();
        emit downloadOk(url,bytes);
    }

    loop.exit();
}

void DownloadManager::stop(bool force){
    reply_busy=false;

    timer.stop();
    loop.exit();

    if (reply->isFinished()!=true and force==false){
        emit downloadError(reply->url(),"timeout");
    }

    reply->close();
}

bool DownloadManager::isBusy(){
    return reply_busy;
}

void DownloadManager::setTimeout(uint value){
    _timeout=value;
}
