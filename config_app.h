#ifndef CONFIG_H
#define CONFIG_H

/*
   xx.xx.xxxx
      - release 1.x.x

   13.11.2019
      - release 1.0.0
*/

#define APP_VERSION "1.1.0 (beta)"
#define APP_DATEBUILD "xx.xx.xxxx"


#endif // CONFIG_H
