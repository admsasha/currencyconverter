#ifndef FORMSETTINGS_H
#define FORMSETTINGS_H

#include <QDialog>

namespace Ui {
class FormSettings;
}

class FormSettings : public QDialog
{
    Q_OBJECT

public:
    explicit FormSettings(QWidget *parent = nullptr);
    ~FormSettings();

private:
    Ui::FormSettings *ui;
};

#endif // FORMSETTINGS_H
